## Standard report for DHIS2 based information system for EWARS

1. The **EpiWeek** Report.html file to be uploaded as a design file.
2. The **ewars-custom-report-assets** directory to be hosted along with dhis war file.

DHIS2 version: 2.30